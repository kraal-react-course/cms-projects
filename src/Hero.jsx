import heroImg from "./assets/dev.svg"

const Hero = () => {
  return (
    <section className="hero">
        <div className="hero-center">
            <div className="hero-title">
                <h1>React Course Projects</h1>
                <p>These are my practice projects created during the React course.<br></br>I made this portfolio page using Contentful CMS (API).</p>
            </div>
            <div className="img-container">
                <img src={heroImg} alt="woman and the browser" />
            </div>
        </div>
    </section>
  )
};
export default Hero;