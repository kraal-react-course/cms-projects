const projects = [
  {
    title: 'bestsellers',
    url: 'https://react-course-1-bestsellers.netlify.app/',
    image: './assets/bestsellers.png',
  },
  {
    title: 'birthday counter',
    url: 'https://react-vite-projects-1-birthday-buddy.netlify.app/',
    image: './assets/birthday.png',
  },
  {
    title: 'tours',
    url: 'https://react-course-2-travel-app.netlify.app/',
    image: './assets/tours.png',
  },
  {
    title: 'reviews',
    url: 'https://react-vite-projects-3-reviews.netlify.app/',
    image: './assets/reviews.png',
  },
  {
    title: 'questions',
    url: 'https://react-vite-projects-4-accordion.netlify.app/',
    image: './assets/questions.png',
  },
  {
    title: 'gallery',
    url: 'https://vite-image-gallery.netlify.app/',
    image: './assets/gallery.png',
  },
];
